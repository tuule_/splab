#include <jni.h>
#include "string.h"


JNIEXPORT jint JNICALL
Java_com_splab_tuule_lab6_NativeWrapper_sum(JNIEnv *env, jclass type, jint a, jint b) {
    return a + b;
}

JNIEXPORT void JNICALL
Java_com_splab_tuule_lab6_NativeWrapper_nativeToast(JNIEnv *jenv, jclass type) {
    jclass cls = (*jenv)->FindClass(jenv, "com/splab/tuule/lab6/NativeWrapper");
    jmethodID methodid = (*jenv)->GetStaticMethodID(jenv, cls, "showToast",
                                                    "(Ljava/lang/String;)V");
    if (!methodid) {
        return;
    }
    jstring jstr = (*jenv)->NewStringUTF(jenv, "Incorrect password, blocked mode!");
    (*jenv)->CallStaticVoidMethod(jenv, cls, methodid, jstr);
}

const char *password = "qweasd123";

JNIEXPORT jboolean JNICALL
Java_com_splab_tuule_lab6_NativeWrapper_checkPassword(JNIEnv *env, jclass type, jstring message_) {
    const char *message = (*env)->GetStringUTFChars(env, message_, 0);
    if (strcmp(message, password) == 0) {
        (*env)->ReleaseStringUTFChars(env, message_, message);
        return JNI_TRUE;
    } else {
        Java_com_splab_tuule_lab6_NativeWrapper_nativeToast(env, type);
        (*env)->ReleaseStringUTFChars(env, message_, message);
        return JNI_FALSE;
    }

}