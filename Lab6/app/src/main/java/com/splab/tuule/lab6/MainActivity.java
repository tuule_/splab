package com.splab.tuule.lab6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NativeWrapper.init(this);
        findViewById(R.id.showToastButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NativeWrapper.nativeToast();
            }
        });

        final EditText aInput = (EditText) findViewById(R.id.inputA);
        final EditText bInput = (EditText) findViewById(R.id.inputB);
        final TextView result = (TextView) findViewById(R.id.jniResult);

        findViewById(R.id.calculateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setText(String.valueOf(NativeWrapper.sum(
                        Integer.valueOf(aInput.getText().toString()),
                        Integer.valueOf(bInput.getText().toString()))));
            }
        });

        final EditText password = (EditText)  findViewById(R.id.passwordET);

        findViewById(R.id.checkPasswordButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NativeWrapper.checkPassword(password.getText().toString())){
                    Toast.makeText(MainActivity.this, "Password correct", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
