package com.splab.tuule.lab6;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by tuule on 03.06.16.
 */
public class NativeWrapper {
    static {
        System.loadLibrary("lab_jni");
    }

    static Context context;

    public static native int sum(int a, int b);

    public static native void nativeToast();

    public static native boolean checkPassword(String message);

    public static void init(Context context){
        NativeWrapper.context = context;
    }

    private static void showToast(String content){
        if (context != null){
            Toast.makeText(context, content, Toast.LENGTH_LONG).show();
        }
    }


}
