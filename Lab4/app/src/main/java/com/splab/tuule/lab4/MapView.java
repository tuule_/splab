package com.splab.tuule.lab4;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by tuule on 02.06.16.
 */
public class MapView extends View {
    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    float rotateAngle = 0;
    float scale = 1;
    Paint roadPaint = new Paint();
    Paint objectPaint = new Paint();

    {
        roadPaint.setStrokeWidth(40);
        roadPaint.setColor(Color.BLUE);
        objectPaint.setColor(Color.RED);

        roadPaint.setAntiAlias(true);
        objectPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.rotate(rotateAngle, canvas.getWidth() / 2, canvas.getHeight() / 2);
        canvas.scale(scale, scale, canvas.getWidth() / 2, canvas.getHeight() / 2);
        canvas.drawLine(getWidth() / 2, -800, getWidth() / 2, getHeight() + 800, roadPaint);
        canvas.drawLine(-800, getHeight() / 2, getWidth() + 800, getHeight() / 2, roadPaint);
        canvas.drawLine(getWidth() / 2, getHeight() / 4, getWidth() + 800, getHeight() / 4, roadPaint);

        canvas.drawRect(50, 50, getWidth() / 2 - 90, getHeight() / 2 - 90, objectPaint);

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event) | scaleGestureDetector.onTouchEvent(event);
    }

    RotateGestureDetector gestureDetector = new RotateGestureDetector(new RotateGestureDetector.OnRotationGestureListener() {
        @Override
        public void OnRotation(RotateGestureDetector rotationDetector) {
            scale = 1;
            if (rotationDetector.getAngle() != 0)
                rotateAngle = -rotationDetector.getAngle();
            if (rotateChangedListener != null) {
                rotateChangedListener.onRotate(rotateAngle);
            }
            invalidate();
        }
    });

    public interface OnRotateChangedListener{
        void onRotate(float angle);
    }

    OnRotateChangedListener rotateChangedListener;

    public void setRotateChangedListener(OnRotateChangedListener rotateChangedListener) {
        this.rotateChangedListener = rotateChangedListener;
    }

    ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.OnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if (detector.getScaleFactor() > 1 || scale > 1)
                scale = detector.getScaleFactor();
            invalidate();
            return false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

        }
    });
}
