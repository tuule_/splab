package com.splab.tuule.lab4;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by tuule on 02.06.16.
 */
public class RotatableImageView extends ImageView {
    public RotatableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    float rotation = 0;

    @Override
    public void setRotation(float rotation) {
        this.rotation = rotation;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.rotate(rotation, getWidth() / 2, getHeight() /2 );
        super.onDraw(canvas);
    }
}
