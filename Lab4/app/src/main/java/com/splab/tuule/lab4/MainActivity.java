package com.splab.tuule.lab4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RotatableImageView imageView = (RotatableImageView) findViewById(R.id.compass);
        ((MapView) findViewById(R.id.map)).setRotateChangedListener(new MapView.OnRotateChangedListener() {
            @Override
            public void onRotate(float angle) {
                imageView.setRotation(angle);
            }
        });
    }
}
