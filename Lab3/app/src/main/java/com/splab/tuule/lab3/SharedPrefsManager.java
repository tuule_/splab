package com.splab.tuule.lab3;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;

/**
 * Created by tuule on 28.05.16.
 */
public class SharedPrefsManager {
    private static SharedPrefsManager ourInstance = new SharedPrefsManager();
    private Context context;
    private SharedPreferences preferences;
    private Properties properties;
    private ArrayList<String> propertyNames = new ArrayList<>();

    public static SharedPrefsManager getInstance() {
        return ourInstance;
    }

    private SharedPrefsManager() {
    }

    public void init(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        properties = System.getProperties();
        Enumeration<?> enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            propertyNames.add(key);
        }
    }

    public ArrayList<String> getPropertyNames() {
        return propertyNames;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }

    public ArrayList<String> searchByName(String query) {
        ArrayList<String> result = new ArrayList<>();
        for (String property :
                propertyNames) {
            if (property.toLowerCase().contains(query.toLowerCase())){
                result.add(property);
            }
        }
        return result;
    }

    public void saveStringValue(String key, String value){
        preferences.edit().putString(key, value).apply();
    }

    public ArrayList<String> getRandomPrefsWithValue(){
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            int rdm = (int) (Math.random() * propertyNames.size());
            result.add(getPropertyNames().get(rdm) + ":" + getValue(getPropertyNames().get(rdm)));
        }
        return result;
    }

    public String getStringValue(String key){
        return preferences.getString(key, UserActions.ACTION_NONE.getStringValue());
    }

    public enum UserActions {
        ACTION_GET_ALL_NAMES("ALL_NAMES"),
        ACTION_GET_ALL_VALUES("ALL_VALUES"),
        ACTION_GET_RANDOM("RANDOM"),
        ACTION_GET_VENDOR("VENDOR"),
        ACTION_NONE("NONE");

        String value;
        UserActions(String value) {
            this.value = value;
        }

        public String getStringValue(){
            return value;
        }
    }
}
