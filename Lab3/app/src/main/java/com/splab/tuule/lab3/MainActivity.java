package com.splab.tuule.lab3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private static final String LAST_ACTION = "lastAction";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPrefsManager.getInstance().init(this);

        final TextView mainField = (TextView) findViewById(R.id.resultField);
        final EditText nameFilterET = (EditText) findViewById(R.id.nameFilterEditText);
        final EditText valueFilterET = (EditText) findViewById(R.id.valuesFromEditText);

        final View filter = findViewById(R.id.nameFilterButton);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefsManager.getInstance().saveStringValue(LAST_ACTION, SharedPrefsManager.UserActions.ACTION_GET_ALL_NAMES.getStringValue());
                mainField.setText(formatText(SharedPrefsManager
                        .getInstance()
                        .searchByName(nameFilterET
                                .getText()
                                .toString())));
            }
        });

        final View values = findViewById(R.id.valuesFromButton);
        values.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefsManager.getInstance().saveStringValue(LAST_ACTION, SharedPrefsManager.UserActions.ACTION_GET_ALL_VALUES.getStringValue());
                ArrayList<String> values = new ArrayList<String>();
                for (String key :
                        SharedPrefsManager.getInstance().searchByName(valueFilterET.getText().toString())) {
                    values.add(SharedPrefsManager.getInstance().getValue(key));
                }
                mainField.setText(formatText(values));
            }
        });

        final View random = findViewById(R.id.getRandomButton);
        random.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefsManager.getInstance().saveStringValue(LAST_ACTION, SharedPrefsManager.UserActions.ACTION_GET_RANDOM.getStringValue());

                mainField.setText(formatText(SharedPrefsManager.getInstance().getRandomPrefsWithValue()));
            }
        });

        final View vendor = findViewById(R.id.getVendorButton);
        vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefsManager.getInstance().saveStringValue(LAST_ACTION, SharedPrefsManager.UserActions.ACTION_GET_VENDOR.getStringValue());
                mainField.setText(SharedPrefsManager.getInstance().getValue("java.vm.specification.vendor"));
            }
        });

        vendor.post(new Runnable() {
            @Override
            public void run() {
                String action = SharedPrefsManager.getInstance().getStringValue(LAST_ACTION);
                if (action != null && !TextUtils.isEmpty(action)){
                    if (action.equals(SharedPrefsManager.UserActions.ACTION_GET_ALL_NAMES.getStringValue())){
                        filter.callOnClick();
                    } else if (action.equals(SharedPrefsManager.UserActions.ACTION_GET_ALL_VALUES.getStringValue())) {
                        values.callOnClick();
                    } else if (action.equals(SharedPrefsManager.UserActions.ACTION_GET_RANDOM.getStringValue())) {
                        random.callOnClick();
                    } else if (action.equals(SharedPrefsManager.UserActions.ACTION_GET_VENDOR.getStringValue())) {
                        vendor.callOnClick();
                    }
                }

            }
        });
    }



    private String formatText(ArrayList<String> results) {
        String res = "";
        for (String string :
                results) {
            res = res.concat(string + "\n");
        }
        return res;
    }
}
