package com.splab.tuule.lab10;

import java.util.ArrayList;

/**
 * Created by tuule on 09.06.16.
 */
public class CloneableItem implements Cloneable {
    String first;
    String second;
    String third;
    String fifth;
    ArrayList<String> strings = new ArrayList<>();

    public CloneableItem(String first, String second, String third, String fifth) {
        this.first = first;
        this.second = second;
        this.third = third;
        this.fifth = fifth;
        strings.add(fifth);
        strings.add(second);
        strings.add(first);
    }

    public String getFirst() {
        return first;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        CloneableItem newCloneableItem = (CloneableItem)super.clone();
        ArrayList<String> arrayList = new ArrayList<>();
        for (String string :
                strings) {
            arrayList.add(string);
        }
        newCloneableItem.strings = arrayList;
        return newCloneableItem;
    }
}
