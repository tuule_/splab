package com.splab.tuule.lab9;

/**
 * Created by tuule on 09.06.16.
 */
public interface Censor {
    String filter(String inputData) throws InterruptedException;
}
