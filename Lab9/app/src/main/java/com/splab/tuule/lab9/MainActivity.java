package com.splab.tuule.lab9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText input;
    private TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.censorEditText);
        output = (TextView) findViewById(R.id.resultText);

        findViewById(R.id.autoCensor).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Censor censorMethod = null;
        final String inputData = input.getText().toString();
        switch (v.getId()) {
            case R.id.dummy:
                censorMethod = new DummyCensor();
                break;

            case R.id.moderation:
                censorMethod = new ModeratorCensor(MainActivity.this);
                break;

            case R.id.autoCensor:
                censorMethod = new AutoCensor();
                break;
        }

        if (censorMethod != null) {
            final Censor finalCensorMethod = censorMethod;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final String result = finalCensorMethod.filter(inputData);
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                output.setText(result);
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
