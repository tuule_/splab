package com.splab.tuule.lab9;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tuule on 09.06.16.
 */
public class AutoCensor implements Censor {
    ArrayList<String> filterMap = new ArrayList<>();
    {
        filterMap.add("гуманитарий");
        filterMap.add("windows");
        filterMap.add("цензура");
        filterMap.add("двойка");
    }

    char tick = 0x2713;

    @Override
    public String filter(String inputData) {
        for (String filter :
                filterMap) {
            if (inputData.contains(filter)){
                return inputData.replaceAll(filter, "***");
            }
        }
        return inputData +" "+ tick;
    }
}
