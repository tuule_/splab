package com.splab.tuule.lab9;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.concurrent.Semaphore;

/**
 * Created by tuule on 09.06.16.
 */
public class ModeratorCensor implements Censor {

    private final Context context;
    private final Handler handler;
    Semaphore semaphore = new Semaphore(1);

    public ModeratorCensor(Context context){
        this.context = context;
        this.handler = new Handler(context.getMainLooper());
    }

    @Override
    public String filter(final String inputData) throws InterruptedException {
        semaphore.acquire();
        final String[] resultString = new String[1];
        handler.post(new Runnable() {
            @Override
            public void run() {
                View dialogView = LayoutInflater.from(context).inflate(R.layout.moderator_window, null);
                final EditText moderatorET = (EditText) dialogView.findViewById(R.id.censorEditText);
                moderatorET.setText(inputData);
                dialogView.findViewById(R.id.submitButton).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resultString[0] = moderatorET.getText().toString();
                        semaphore.release();
                    }
                });
                new AlertDialog.Builder(context)
                        .setTitle("Edit this!")
                        .setView(dialogView)
                        .create()
                        .show();
            }
        });
        semaphore.acquire();
        return resultString[0];
    }
}
