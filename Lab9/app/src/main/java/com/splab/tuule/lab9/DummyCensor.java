package com.splab.tuule.lab9;

/**
 * Created by tuule on 09.06.16.
 */
public class DummyCensor implements Censor {
    @Override
    public String filter(String inputData) throws InterruptedException {
        return "Remove your ADBlock, Please!";
    }
}
