package com.splab.tuule.lab5;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class MainActivity extends AppCompatActivity {

    final static int REQ_CODE_PICK_IMAGE_FIRST = 35;
    final static int REQ_CODE_PICK_IMAGE_SECOND = 42;

    private ImageView resultImage;
    private ImageView firstImage;
    private ImageView secondImage;
    ProgressDialog processingDialog;
    private volatile Bitmap resultBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultImage = (ImageView) findViewById(R.id.resultImage);
        firstImage = (ImageView) findViewById(R.id.firstImage);
        secondImage = (ImageView) findViewById(R.id.secondImage);
        processingDialog = new ProgressDialog(this);
        processingDialog.setIndeterminate(true);
        processingDialog.setCancelable(false);
        processingDialog.setTitle("Processing…");
        resultImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resultImage != null) {
                    storeImage(resultBitmap);
                    Toast.makeText(MainActivity.this, "Saved to gallery", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void getPhotoFromGallery(int num) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, num);
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (resultCode == RESULT_OK) {
            Uri selectedImage = imageReturnedIntent.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);

            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            if (requestCode == REQ_CODE_PICK_IMAGE_FIRST)
                firstImage.setImageBitmap(bitmap);
            else
                secondImage.setImageBitmap(bitmap);
            cursor.close();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_first_image) {
            getPhotoFromGallery(REQ_CODE_PICK_IMAGE_FIRST);
        } else if (item.getItemId() == R.id.action_second_image) {
            getPhotoFromGallery(REQ_CODE_PICK_IMAGE_SECOND);
        } else if (item.getItemId() == R.id.action_process) {
            processImages();
        }
        return super.onOptionsItemSelected(item);
    }

    ExecutorService executorService = Executors.newSingleThreadExecutor();


    public void processImages() {
        processingDialog.show();
        executorService.submit(new Runnable() {
            @Override
            public void run() {

                Bitmap firstBitmap = ((BitmapDrawable) firstImage.getDrawable()).getBitmap();
                Bitmap secondBitmap = ((BitmapDrawable) secondImage.getDrawable()).getBitmap();

                if (firstBitmap == null) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            processingDialog.hide();
                            Toast.makeText(MainActivity.this, "No first image", Toast.LENGTH_SHORT).show();
                        }
                    });

                    return;
                }

                if (secondBitmap == null) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            processingDialog.hide();

                            Toast.makeText(MainActivity.this, "No second image", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }


                if (firstBitmap.getHeight() <= secondBitmap.getHeight())
                    resultBitmap = Bitmap.createBitmap(firstBitmap.getWidth(), firstBitmap.getHeight(), Bitmap.Config.RGB_565);
                else
                    resultBitmap = Bitmap.createBitmap(secondBitmap.getWidth(), secondBitmap.getHeight(), Bitmap.Config.RGB_565);

                Canvas c = new Canvas(resultBitmap);
                Paint pixelPaint = new Paint();

                for (int x = 0; x < resultBitmap.getWidth(); x++) {
                    for (int y = 0; y < resultBitmap.getHeight(); y++) {
                        int firstPixelColor = firstBitmap.getPixel(x, y);
                        int secondPixelColor = secondBitmap.getPixel(x, y);
                        pixelPaint.setColor(mixColors(firstPixelColor, secondPixelColor, y % 2 == 0));
                        c.drawPoint(x, y, pixelPaint);
                    }
                }

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        processingDialog.hide();
                        resultImage.setImageBitmap(resultBitmap);
                    }
                });
            }
        });
    }

    public int mixColors(int first, int second, boolean invert) {
        int resultRed = invert ? 255 - (Color.red(first) + Color.red(second)) / 2 : (Color.red(first) + Color.red(second)) / 2;
        int resultGreen = invert ? 255 - (Color.green(first) + Color.green(second)) / 2 : (Color.green(first) + Color.green(second)) / 2;
        int resultBlue = invert ? 255 - (Color.blue(first) + Color.blue(second)) / 2 : (Color.blue(first) + Color.blue(second)) / 2;
        return Color.rgb(resultRed, resultGreen, resultBlue);
    }
}
