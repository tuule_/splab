package com.splab.tuule.lab7;

import android.content.Context;
import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Created by tuule on 03.06.16.
 */
public class MatrixManager {
    private static MatrixManager ourInstance = new MatrixManager();
    private Context context;

    public static MatrixManager getInstance() {
        return ourInstance;
    }

    private MatrixManager() {
    }

    ExecutorService executor = Executors.newCachedThreadPool();

    public static final String SOURCES_FILENAME = "sources.mtrx";
    private ArrayList<Matrix> matrixArray = new ArrayList<>();
    Semaphore writeSemaphore = new Semaphore(1);
    Semaphore readSemaphore = new Semaphore(1);


    public void init(Context context) {
        this.context = context;
        updateMatrixFromFile(null);
    }

    private Matrix generateMatrix(int quantity) {
        Random random = new Random();
        double[][] matrix = new double[quantity][quantity];
        for (int i = 0; i < quantity; i++)
            for (int j = 0; j < quantity; j++)
                matrix[i][j] = random.nextInt();
        return new Matrix(matrix);
    }


    public void saveNewMatrixPair(final AsyncOperationCallback callback) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    writeSemaphore.acquire();
                    matrixArray.add(generateMatrix(200));
                    matrixArray.add(generateMatrix(200));
                    IOHelper.getInstance().writeToFile(SOURCES_FILENAME, matrixArray);
                    writeSemaphore.release();
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFail();
                    }
                }
            }
        });
    }

    private void updateMatrixFromFile(final AsyncOperationCallback callback) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    readSemaphore.acquire();
                    matrixArray = (ArrayList<Matrix>) IOHelper.getInstance().getFileData(SOURCES_FILENAME);
                    readSemaphore.release();
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFail();
                    }
                }
            }
        });
    }

    int offset = 0;

    public void saveResult(final Matrix matrix, final String identifier, final AsyncOperationCallback callback) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                IOHelper.getInstance().writeToFile(identifier, matrix);
                if (callback != null) {
                    callback.onSuccess(null);
                }
            }
        });
    }

    @Nullable
    public Matrix[] getMatricies() {
        try {
            Matrix[] matrices = new Matrix[2];
            matrices[0] = matrixArray.get(offset++);
            matrices[1] = matrixArray.get(offset++);
            return matrices;
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }
}
