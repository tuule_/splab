package com.splab.tuule.lab7;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Base64;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.concurrent.Semaphore;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


public class IOHelper {
    private static IOHelper instance = new IOHelper();
    private Context context;

    public static IOHelper getInstance() {
        return instance;
    }

    private IOHelper() {
    }

    public void init(Context context) {
        this.context = context;
    }

    ;


    public void writeToFile(String filename, Object data) {
        try {
            FileOutputStream outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            OutputStream buffer = new BufferedOutputStream(outputStream);
            ObjectOutput output = new ObjectOutputStream(buffer);
            output.writeObject(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Object getFileData(String filename) {
        Matrix result = null;
        try {
            FileInputStream inputStream = context.openFileInput(getFileAbsolutePath(filename));
            InputStream buffer = new BufferedInputStream(inputStream);
            ObjectInput input = new ObjectInputStream(buffer);
            result = (Matrix) input.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }


    private String getFileAbsolutePath(String filename) {
        return context.getFilesDir().getAbsolutePath() + File.separator + filename;
    }
}
