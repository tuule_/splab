package com.splab.tuule.lab7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MatrixManager.getInstance().init(this);
        IOHelper.getInstance().init(this);
        startCalculation((TextView) findViewById(R.id.result));
        startCalculation((TextView) findViewById(R.id.result2));
        startCalculation((TextView) findViewById(R.id.result3));
        startCalculation((TextView) findViewById(R.id.result4));
        startCalculation((TextView) findViewById(R.id.result5));
        startCalculation((TextView) findViewById(R.id.result6));
        startCalculation((TextView) findViewById(R.id.result7));
        startCalculation((TextView) findViewById(R.id.result8));
        startCalculation((TextView) findViewById(R.id.result9));
        startCalculation((TextView) findViewById(R.id.result10));
        startCalculation((TextView) findViewById(R.id.result11));
        startCalculation((TextView) findViewById(R.id.result12));
        startCalculation((TextView) findViewById(R.id.result13));
        startCalculation((TextView) findViewById(R.id.result14));
        startCalculation((TextView) findViewById(R.id.result15));
        startCalculation((TextView) findViewById(R.id.result16));
        startCalculation((TextView) findViewById(R.id.result17));
        startCalculation((TextView) findViewById(R.id.result18));
        startCalculation((TextView) findViewById(R.id.result19));
        startCalculation((TextView) findViewById(R.id.result20));

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void startCalculation(final TextView label){
        MatrixManager.getInstance().saveNewMatrixPair(new AsyncOperationCallback() {
            @Override
            public void onSuccess(Object resultMatrix) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        label.setText("Calculating");
                    }
                });

                AsyncCalculator.getInstance().calculateMatricies(
                        MatrixManager.getInstance().getMatricies(),
                        new AsyncOperationCallback<Matrix>() {
                            @Override
                            public void onSuccess(final Matrix resultMatrix) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        label.setText("Success");
                                    }
                                });

                                MatrixManager.getInstance().saveResult(resultMatrix, label.toString(), new AsyncOperationCallback() {
                                    @Override
                                    public void onSuccess(Object resultMatrix) {
                                    }

                                    @Override
                                    public void onFail() {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                label.setText("Fail");
                                            }
                                        });

                                    }
                                });
                            }

                            @Override
                            public void onFail() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        label.setText("Fail");
                                    }
                                });


                            }
                        });
            }

            @Override
            public void onFail() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        label.setText("Fail");
                    }
                });

            }
        });

    }
}
