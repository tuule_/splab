package com.splab.tuule.lab7;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by tuule on 04.06.16.
 */
public class AsyncCalculator {
    private static AsyncCalculator ourInstance = new AsyncCalculator();

    public static AsyncCalculator getInstance() {
        return ourInstance;
    }

    private AsyncCalculator() {
    }

    ExecutorService executorService = Executors.newFixedThreadPool(14);


    public void calculateMatricies(Matrix[] matrices, final AsyncOperationCallback<Matrix> callback) {
        final Matrix first = matrices[0];
        final Matrix second = matrices[1];
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                int aRows = first.getArray().length;
                int aColumns = first.getArray()[0].length;
                int bRows = second.getArray().length;
                int bColumns = second.getArray()[0].length;

                if (aColumns != bRows) {
                    if (callback != null) {
                        callback.onFail();
                    }
                    return;
                }

                double[][] resultArray = new double[aRows][bColumns];
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        resultArray[i][j] = 0.00000;
                    }
                }

                for (int i = 0; i < aRows; i++) { // aRow
                    for (int j = 0; j < bColumns; j++) { // bColumn
                        for (int k = 0; k < aColumns; k++) { // aColumn
                            resultArray[i][j] += first.getArray()[i][k] * second.getArray()[k][j];
                        }
                    }
                }


                callback.onSuccess(new Matrix(resultArray));
            }
        });
    }

}
