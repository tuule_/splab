package com.splab.tuule.lab7;

import java.io.Serializable;

/**
 * Created by tuule on 03.06.16.
 */
public class Matrix implements Serializable {
    private final double[][] array;
    public Matrix(double[][] array){
        this.array = array;
    }

    public double[][] getArray() {
        return array;
    }
}
