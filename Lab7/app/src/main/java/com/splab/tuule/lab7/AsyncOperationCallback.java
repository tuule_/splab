package com.splab.tuule.lab7;

public interface AsyncOperationCallback<E> {
    void onSuccess(E resultMatrix);

    void onFail();
}
