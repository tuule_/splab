package com.splab.tuule.lab8;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    ExecutorService executorService = Executors.newCachedThreadPool();
    ProgressDialog processing;
    private DataInputStream dis;
    public DataOutputStream dos;
    private boolean isConnected;
    private TextView responses;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        processing = new ProgressDialog(this);
        processing.setIndeterminate(false);
        processing.setCancelable(false);

        final EditText addr = (EditText) findViewById(R.id.ipaddr);
        final EditText port = (EditText) findViewById(R.id.port);

        findViewById(R.id.connectButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processing.setTitle("Connecting…");
                processing.show();

                executorService.submit(new Runnable() {
                    @Override
                    public void run() {
                        Socket s = null;
                        try {
                            String address = addr.getText().toString();
                            int portSock = Integer.parseInt(port.getText().toString());
                            s = initSocket(address, portSock);
                            isConnected = s.isConnected();
                        } catch (Exception e) {
                            e.printStackTrace();
                            hideProgress();
                        }
                        try {
                            dis = getDataInputStream(s);
                            dos = getDataOutputStream(s);
                        } catch (IOException e) {
                            e.printStackTrace();
                            hideProgress();
                        }
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                processing.hide();
                            }
                        });
                    }
                });
            }
        });

        final EditText messages = (EditText) findViewById(R.id.data);

        findViewById(R.id.sendData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executorService.submit(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            dos.writeUTF(messages.getText().toString() + "\n");
                            dos.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        try {
                            addData(dis.readUTF());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        responses = (TextView) findViewById(R.id.responces);
    }

    private void addData(String response) {
        String data = responses.getText().toString();
        data = response + "\n" + data;
        responses.setText(data);
    }

    private void hideProgress() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                processing.hide();
            }
        });
    }

    private Socket initSocket(String addr, int port) throws Exception {
        InetAddress address = InetAddress.getByName(addr);
        return new Socket(address, port);
    }

    //private String

    private DataInputStream getDataInputStream(Socket socket) throws IOException {
        return new DataInputStream(socket.getInputStream());
    }

    private DataOutputStream getDataOutputStream(Socket socket) throws IOException {
        return new DataOutputStream(socket.getOutputStream());
    }


}
