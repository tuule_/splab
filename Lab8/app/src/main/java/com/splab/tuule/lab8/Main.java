package com.splab.tuule.lab8;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by tuule on 03.06.16.
 */
public class Main {
    static ExecutorService executorService = Executors.newCachedThreadPool();

    public static void main(String[] args) {
        ServerSocket server = null;
        try {
            server = new ServerSocket(4444);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("Cannot start server, shutting down");
            System.exit(-1);
        }

        startServer(server);

    }

    private static void startServer(final ServerSocket server) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                Socket fromClient = null;
                try {
                    System.out.println("Waiting for a client...");
                    fromClient = server.accept();
                    startServer(server);
                    System.out.println("Client connected");
                } catch (IOException e) {
                    System.out.println("Can't accept");
                    System.exit(-1);
                }

                BufferedReader in = null;
                DataOutputStream out = null;
                try {
                    in = new BufferedReader(new
                            InputStreamReader(fromClient.getInputStream()));
                    out = new DataOutputStream(fromClient.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("Error while opening data streams");
                    System.exit(-1);
                }

                String input, output;

                try {
                    while ((input = in.readLine()) != null) {
                        if (input.equalsIgnoreCase("exit")) break;
                        input = input.substring(2, input.length());
                        System.out.println("Client said: " + input);
                        String[] stringCoeffs = input.split(",");
                        output = "";
                        for (int i = 0; i < stringCoeffs.length; i++) {
                            try {
                                output += Integer.parseInt(stringCoeffs[i]) * i + " ";
                            } catch (NumberFormatException e) {
                                output = "wrong data format";
                                break;
                            }
                        }


                        out.writeUTF(output);
                        out.flush();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("Error while parsing data");
                    System.exit(-1);
                }
            }
        });
    }
}
